import asyncHandler from "express-async-handler";
import Order from "../models/orderModel.js";

const addOrderItems = asyncHandler(async (req, res) => {
  const {
    orderItems,
    shippingAddress,
    itemsPrice,
    taxPrice,
    shippingPrice,
    totalPrice,
  } = req.body;

  if (orderItems && orderItems.length === 0) {
    res.status(400);
    throw new Error(" No order items");
  } else {
    const order = await Order.create({
      orderItems,
      user: req.user._id,
      shippingAddress,
      itemsPrice,
      taxPrice,
      shippingPrice,
      totalPrice,
    });

    const createdOrder = await order.save();
    res.status(201).json(createdOrder);
  }
});

const getOrderById = asyncHandler(async (req, res) => {
  const order = await Order.findById(req.params.id).populate(
    "user",
    "name email"
  );
  if (order) {
    res.json(order);
  } else {
    res.status(404);
    throw new Error("Order not found");
  }
});

const markOrdersPaid = async (req, res) => {
  const order = await Order.findById(req.params.id);
  if (order) {
    order.isPaid = true;
    order.paidAt = Date.now();
    order.paymentResult = {
      id: req.body.id,
      status: req.body.status,
      update_time: req.body.update_time,
      email: req.body.email,
    };

    const updatedOrder = await order.save();
    res.json(updatedOrder);
  } else {
    res.status(400).json({ message: "Order not found" });
  }
};

const markOrdersDelievered = async (req, res) => {
  const order = await Order.findById(req.params.id);
  if (order) {
    order.isDelievered = true;
    order.delieveredAt = Date.now();

    const updatedOrder = await order.save();
    res.json(updatedOrder);
  } else {
    res.status(400).json({ message: "Order not found" });
  }
};

const getOrdersList = async (req, res) => {
  try {
    const orders = await Order.find({});
    if (orders) {
      res.status(200).json({ orders });
    } else {
      res.status(400).json({ message: "No orders found" });
    }
  } catch (error) {
    res.send({ error });
  }
};

const getPersonalOrders = async (req, res) => {
  const orders = await Order.find({ user: req.user._id });
  res.json(orders);
};

const deleteOrder = async (req, res) => {
  try {
    const removed = await Order.findOneAndRemove({
      _id: req.params.id,
    });

    if (!removed) {
      return res.status(400).end();
    }

    return res.status(200).json({ data: removed });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

export {
  addOrderItems,
  getOrderById,
  getOrdersList,
  getPersonalOrders,
  markOrdersDelievered,
  markOrdersPaid,
  deleteOrder,
};
