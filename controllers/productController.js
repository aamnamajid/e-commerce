import Products from "../models/productModel.js";
import cloudinary from "cloudinary";

cloudinary.config({
  cloud_name: "dssjvtnz1",
  api_key: "529361283449248",
  api_secret: "Qm2xhrgf2OU1OZ243AaGTBnge_k",
  secure: true,
});

// get list of all products
const getProducts = async (req, res) => {
  const pageSize = 8;
  const page = Number(req.query.pageNumber) || 1;

  const keyword = req.query.keyword
    ? {
        $or: [
          { name: { $regex: req.query.keyword, $options: "i" } },
          { category: { $regex: req.query.keyword, $options: "i" } },
        ],
      }
    : {};

  const totalCount = await Products.count({ ...keyword });
  const products = await Products.find({ ...keyword })
    .limit(pageSize)
    .skip(pageSize * (page - 1));

  res.json({ products, page, pages: Math.ceil(totalCount / pageSize) });
};

const getProductsAdmin = async (req, res) => {
  const products = await Products.find({});
  res.json({ products });
};

// get product by ID
const getProductById = async (req, res) => {
  const product = await Products.findById(req.params.id);
  if (product) {
    res.json(product);
  } else {
    res.status(404).json({ message: "Product not found" });
  }
};

// DEL product by ID
const deleteProduct = async (req, res) => {
  try {
    const removed = await Products.findOneAndRemove({
      _id: req.params.id,
    });

    if (!removed) {
      return res.status(400).end();
    }

    return res.status(200).json({ data: removed });
  } catch (e) {
    console.error(e);
    res.status(400).end();
  }
};

// Update Product
const updateOne = async (req, res) => {
  const product = await Products.findOneAndUpdate(
    { _id: req.params.id },
    req.body,
    { new: true }
  )
    .lean()
    .exec();
  if (!product) {
    return res.status(400).end();
  }

  res.status(200).json({ data: product });
};

// create Product
const createProduct = async (req, res) => {
  const file = req.files.prodImage;
  const uniqueFileName = req.body.code;
  const { name, price, description, code, countInStock, category } = req.body;

  try {
    const image = await cloudinary.uploader.upload(file.tempFilePath, {
      public_id: `images/${uniqueFileName}`,
      tags: "images",
    });
    if (image) {
      const newProduct = new Products({
        name,
        price,
        description,
        rating: 0,
        numReviews: 0,
        code,
        countInStock,
        category,
        prodImage: image.url,
      });

      const response = await newProduct.save();
      if (response) {
        res.status(201).json({
          success: true,
          data: response,
        });
      } else {
        res.status(501).json({
          success: false,
          data: [],
          message: "Error while adding product",
        });
      }
    }
  } catch (error) {
    console.error(error);
  }
};

// add review to Product
const addReview = async (req, res) => {
  const { rating, comment } = req.body;

  const product = await Products.findById(req.params.id);
  if (product) {
    const review = {
      name: req.user.name,
      rating: Number(rating),
      comment,
      user: req.user._id,
    };
    product.reviews.push(review);
    product.numReviews = product.reviews.length;

    product.rating =
      product.reviews.reduce((acc, item) => item.rating + acc, 0) /
      product.reviews.length;

    await product.save();
    res.status(201).json({ message: "Review added" });
  } else {
    res.status(400).json({ message: "Unable to add comment" });
  }
};

const getTopProducts = async (req, res) => {
  const products = await Products.find({}).sort({ rating: -1 }).limit(8);
  res.json(products);
};

export {
  getProductById,
  getProducts,
  deleteProduct,
  createProduct,
  updateOne,
  addReview,
  getTopProducts,
  getProductsAdmin,
};
