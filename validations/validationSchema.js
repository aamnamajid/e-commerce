import Joi from '@hapi/joi'

const registerUserValidation = async(req,res,next) => {
   const userSchema = Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().email().required(),
        password: Joi.string().min(6).required()
    })

    try {
        const result = await userSchema.validateAsync(req.body)
        next()
    } catch (error) {
        if(error.isJoi === true) return res.status(400).json({message: 'Validation failed'})
        next()
    }
 

}

const loginValidator = async(req,res,next) => {
    const userSchema = Joi.object().keys({
         email: Joi.string().email().required(),
         password: Joi.string().min(6).required()
     })
 
     try {
         const result = await userSchema.validateAsync(req.body)
         next()
     } catch (error) {
         if(error.isJoi === true) return res.status(400).json({message: 'Validation failed'})
         next()
     }
  
 
 }
 

export {
    loginValidator,
    registerUserValidation
}