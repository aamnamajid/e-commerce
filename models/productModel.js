import mongoose from 'mongoose'

const reviewsSchema = mongoose.Schema({
    name: {type: String, required: true},
    rating: {type: Number, required: true},
    comment:{ type:String, required: true},
    user:{
        type: mongoose.Schema.Types.ObjectId,
        required: 'true',
        ref: 'User'
    }
},
    {
        timestamps: true
    }
)
const productSchema = mongoose.Schema({

    name: {
        type: String,
        required: true
    },
    prodImage: {
        type: String,
        required: true
    }, 
    code: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    rating: {
        type: Number,
        required: true,
        default: 0
    },
    reviews: [reviewsSchema],
    numReviews: {
        type: Number,
        required: true,
        default: 0
    },
    price: {
        type: Number,
        required: true,
        default: 0
    },
    countInStock: {
        type: Number,
        required: true,
        default: 0
    },
    category: {
        type: String,
        required: true,
    },
},
    {timestamps: true}
)

const Products = mongoose.model('products', productSchema)
export default Products