import express from "express";
import connectDb from "./config/db.js";
import dotenv from 'dotenv'
import morgan from 'morgan'
import fileupload from 'express-fileupload'
import cors from 'cors'
import routes from './routes/index.js'

dotenv.config()
connectDb()

const app = express()
const PORT = process.env.PORT || 5000;

app.use(cors({
    origin: '*',
    credentials: true
}));

app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }));
app.use(fileupload({
    useTempFiles: true
}))

routes.product_Routes(app)
routes.user_Routes(app)
routes.order_Routes(app)

app.listen(PORT, console.log(
    `Server is running on port: ${PORT}`
))