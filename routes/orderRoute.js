import express from "express";
import {
  addOrderItems,
  getOrderById,
  getOrdersList,
  markOrdersPaid,
  markOrdersDelievered,
  getPersonalOrders,
  deleteOrder,
} from "../controllers/orderController.js";
import { protect, admin } from "../middleware/auth.middleware.js";

/*const router = express.Router()
router.route('/').post(protect, addOrderItems).get(protect, admin, getOrdersList)
router.route('/:id').get(protect, getOrderById)
router.route('/:id/paid').put(protect, admin, markOrdersPaid)
router.route('/:id/delievered').put(protect, admin, markOrdersDelievered)
export default router*/

export default function order_Routes(app) {
  app
    .route("/api/order")
    .post(protect, addOrderItems)
    .get(protect, admin, getOrdersList);

  app.get("/api/order/myorders", protect, getPersonalOrders);

  app
    .route("/api/order/:id")
    .get(protect, getOrderById)
    .delete(protect, admin, deleteOrder);
  app.put("/api/order/:id/paid", protect, admin, markOrdersPaid);
  app.put("/api/order/:id/delievered", markOrdersDelievered);
}
