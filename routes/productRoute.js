import {
  getProducts,
  getProductById,
  deleteProduct,
  createProduct,
  updateOne,
  addReview,
  getTopProducts,
  getProductsAdmin,
} from "../controllers/productController.js";
import { protect, admin } from "../middleware/auth.middleware.js";

export default function product_Routes(app) {
  app
    .route("/api/products")
    .get(getProducts)
    .post(protect, admin, createProduct);

  app.route("/api/admin/products").get(protect, admin, getProductsAdmin);

  app.route("/api/products/top").get(getTopProducts);

  app
    .route("/api/products/:id")
    .get(getProductById)
    .delete(protect, admin, deleteProduct)
    .put(protect, admin, updateOne);

  app.route("/api/products/:id/review").post(protect, addReview);
}
