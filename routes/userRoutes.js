import { protect, admin } from "../middleware/auth.middleware.js";
import {
  login,
  getUserProfile,
  registerUser,
  updateUserProfile,
  getAllUsers,
  deleteUser,
  getUserById,
  updateUserByAdmin,
} from "../controllers/userController.js";
import {
  loginValidator,
  registerUserValidation,
} from "../validations/validationSchema.js";

export default function user_Routes(app) {
  app
    .route("/api/users")
    .get(protect, admin, getAllUsers)
    .post(registerUserValidation, registerUser);

  app
    .route("/api/users/profile")
    .get(protect, getUserProfile)
    .put(protect, updateUserProfile);

  app.post("/api/users/login", loginValidator, login);
  app.post("/api/users/create", protect, admin, registerUser);

  app
    .route("/api/users/:id")
    .delete(protect, admin, deleteUser)
    .get(protect, admin, getUserById)
    .put(protect, admin, updateUserByAdmin);
}
