# E-commerce

The project covers E-commerce functionalities which are as follows:
- Product reviews and ratings
- Top products carousel
- Product pagination
- Product search feature
- User profile with orders
- Admin product management
- Admin user management
- Admin Order details page
- Mark orders as delivered option
- Checkout process (shipping, payment method, etc)

# Install Dependencies
npm install

# Run
npm start

# Env Variables
Create a .env file in then root and add the following:
MONGO_URI = 'mongo url'
PORT = 5000
JWT_SECRET = 'secret key'
JWT_EXP = 'expiry'



